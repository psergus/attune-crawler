'use strict';
var _ = require('lodash');

var full_config = {
	debug: false,
	site: 'http://www.bonobos.com',
	navigation: { //we can manually inject navigation to the crawler
		name: 'Categories',
		path: 'nav.site-nav>ul li>a',
		meta: {
			name: {
				func: 'text'
			},
			location: {
				attr: 'href'
			}
		}
	},
	products: {
	    name: 'Category', /* will be replaced with real category name */
	    meta: {
	        'null': {
	        }
	    },
	    elements: {
	        name: 'SubCategory',
	        path: 'section.category-products', /* we can just point to the document's body if there are no sub categories */
	        meta: {
	            name: {
	                path: 'h2',
	                func: 'text'
	            }
	        },
	        elements: {
	            name: 'Products',
	            path: "li[itemtype='http://schema.org/Product']",
	            meta: {
	                id: {
	                    attr: 'id'
	                },
	                location: {
	                    path: 'a',
	                    attr: 'href'
	                },
	                title: {
	                    path: '.product-name',
	                    func: 'text',
	                },
	                description: {
	                    path: '.short-description',
	                    func: 'text'
	                },
	                image: {
	                    path: "img[itemprop='image']",
	                    attr: 'src'
	                },
	                price: {
	                    path: '.price',
	                    func: 'text'
	                }
	            }
	        }
	    }
	}
};
var crawler = require('./lib/crawler.js').getCrawler( full_config );

crawler.crawl(function(err, results) {
	if(err) {
		console.log(err);
	}
	else {
		//console.log( JSON.stringify( results ) );
		//create master list from the results
		var master_list = _.chain( results ).reduce(function(result, collection, key) {
			return result.concat( _.reduce( collection.meta[0].elements.meta, function(res, obj, key) {
				return res.concat( 
					_.map( obj.elements.meta, function( prod ) {
						prod.category = collection.name;
						prod.sub_category = obj.name;
						return prod;
					})
				);
			}, []) );
		}, []).value();

		console.log( master_list );
	}
	console.log('Done');
	process.exit(1);
});
