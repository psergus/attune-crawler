'use strict';
var Browser = require("zombie"),
	jsdom = require('jsdom'),
	fs = require("fs"),
	async = require("async"),
	_ = require('lodash'),
	jquery = fs.readFileSync(__dirname + '/tmp/jquery.js', "utf-8");


//functional
var curry = function(func, context, a) {
	var args = Array.prototype.slice.call(arguments);
	args.shift(); //remove the first argument
	args.shift(); //remove the second argument
	return function(b) {
		var inner_args = Array.prototype.slice.call(arguments);
		return func.apply(context, args.concat( inner_args ) );
	}
}
//remove new lines, and trim the string
String.prototype.normal = function() {
	return this.replace(/(\r\n|\n|\r)/gm," ").replace(/^\s+|\s+$/g, '');
}


/*
* recursive function which takes trhee arguments
* $ - jsdom ( jQuery ) reference to the document
* dom - document element which defines the scope of search for elements
* config - configuration for exploring elements
*/
function grabElements( $, dom, config ) {
	var result = {}; // we will return the object literal containing the elements required from the config
	//grab the scope - where we will find our elements from the config

	var scoped_dom = (config.path !== undefined)? $( dom ).find( config.path ) : dom;

	result.name = config.name;


	//grab meta data
	if( config.meta !== undefined ) {
		//resulr.meta will contain the array of elements with their metadata
		result.meta = [];
		//if it's one element or many elements, we just go through the each one
		$.each( scoped_dom, function(index, el) {
			var element = {};
			for( var i in config.meta ) { //grab the meta data we need to each element
				if(config.meta.hasOwnProperty(i)) {
					//grab properties for each metadata as defined in config
					var prop = (config.meta[i].path !== undefined)? $(el).find( config.meta[i].path ) : $(el);
					if( prop ) {
						//if it's attribute, call attribute
						if(config.meta[i].attr !== undefined) {
							element[i] = prop.attr( config.meta[i].attr );
							if(element[i] === undefined) {
								return true; //skip it
							}
							else {
								element[i] = element[i].normal();
							}
						}
						else if(config.meta[i].func !== undefined ) { // otherwise apply function
							switch( config.meta[i].func ) {
								case 'text':
									element[i] = prop.text().normal();
									break;
								default:
									throw new Error('Function ' + config.meta[i].func + ' is not supported');
									break;
							}
						}
					}

					//this element has sub-elements, lets do a recursive call
					if( config.hasOwnProperty('elements') ) {
						element.elements = grabElements( $, el, config.elements );
					}
				}
			}
			result.meta.push( element );
		});
	}
	return result;
}



module.exports = {
	getCrawler: function( site ) {
		return new Crawler( site );
	}
};

var Crawler = function( config ) {

	config = config || {}; //make sure we have a config

	//tune from the config
	this.site = config.site || null;
	this.debug = config.debug || false;
	this.userAgent = config.userAgent || '';//'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_0) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4';
	this.config_navigation = config.navigation || null;
	this.config_products = config.products || null;

	//properties for internal usage
	this.navigation = null; //possible categories to crawl
	this.categories = {}; //results of category explorations

	//init browser
	this.browser = new Browser({
		silent: true,
	    loadCSS: false,
	    debug: this.debug,
	    site: this.site,
	    userAgent: this.userAgent
	});
};

Crawler.prototype.getBrowser = function() {
	return this.browser;
};

/*
* Inject navigation manually. Should be good for web-sites where it's hard to auto generate the navigation
* Here is an example what can be inserted:
* 
*{
*	name: 'Categories',
*	meta: [
*		{ name: 'Shoes', location: '/shoes' },
*		{ name: 'Blazers', location: '/blazers' },
*		....
*	]
*}
*/
Crawler.prototype.setNavigation = function( navigation ) {
	this.navigation = navigation;
}

/*
* crowl the page, convert the content into jsdom (jQuery) and return it
*/
Crawler.prototype.getDom = function( path, callback ) {
	var $that = this;
	this.getBrowser().visit( path ).
		then( function() {
			var html = $that.getBrowser().html();
			if(html) {
				jsdom.env({
					html: html, 
					src: [jquery],
					done: function (errors, window) {
					    callback( null, window.$);
					}
				});
			}
			else {
				callback( new Error( 'Cannot crawl the page' ), null);
			}
		}).
		fail( function(error) {
			console.log( error );
			callback( error, null);
		});
};

/*
*	auto generate navigation. we can also inject it manually before calling this method
*/
Crawler.prototype.getNavigation = function( callback ) {
	var $that = this;
	if( this.navigation ) {
		callback(null, $that.navigation);
	}
	else {
		if( !this.config_navigation ) {
			callback( new Error('Please provide navigation configuration'), null );
		}
		else {
			this.getDom( '', function( err, $) {
				if(err) {
					callback( err, null);
				}
				else {
					//update site's navigation
					$that.navigation = grabElements( $, $('html'), $that.config_navigation);
					callback(null, $that.navigation );
				}
			});
		}
	}
};

/*
*	discover one category for products
*/
Crawler.prototype.discoverCategory = function( href, category_name, callback ) {
	var $that = this;
	if(this.categories[category_name]) {
		callback(null, this.categories[category_name]);
	}
	else {
		if( !this.config_products ) {
			callback( new Error('Please provide product page navigation'), null);
		}
		else {
			console.log('Explore category ' + category_name + ' at: ' + href);
			this.getDom( href, function( err, $ ) {
				if(err) {
					callback( err, null);
				}
				else {
					try {
						var result = grabElements( $, $('html'), $that.config_products );
						//add collection name and location
						result.location = href;
						result.name = category_name;

						$that.categories[category_name] = result;
						callback(null, result);
					}
					catch(err) {
						console.log('Error: ' + err);
						callback(err, null);
					}
				}
			});
		}
	}
};

/*
* 	the function will discover navigation and crawl through all categories, returning the final result
*/
Crawler.prototype.crawl = function(callback) {
	var $that = this;
	this.getNavigation( function(err, navigation) {
		if(err) {
			callback(err, null);
		}
		else {
			//console.log(navigation);
			//all ok, continue
			var calls = []; //we are going to serialize the query of calls to discover each category
			_(navigation.meta).each(function( category ) {
				calls.push( curry( $that.discoverCategory, $that, category.location, category.name ) );
			});

			//and now we just let each go
			async.series(calls, function(err, results) {
				console.log('Got all results');
				var results = _.chain(results).flatten().remove( function ( o ) { return /*Object.keys(o).length !== 0*/ o.meta[0].elements.meta.length !== 0 } ).value();
				callback( null, results );
			});
		}
	});
}
