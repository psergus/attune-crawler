'use strict';

var _ = require('lodash');
var site = 'http://www.bonobos.com';

var navigation_only_config = {
	debug: false,
	site: site,
	navigation: {
		name: 'Categories',
		path: 'nav.site-nav>ul li>a',
		meta: {
			name: {
				func: 'text'
			},
			location: {
				attr: 'href'
			}
		}
	}
};

var full_config = {
	debug: false,
	site: site,
	navigation: {
		name: 'Categories',
		path: 'nav.site-nav>ul li>a',
		meta: {
			name: {
				func: 'text'
			},
			location: {
				attr: 'href'
			}
		}
	},
	products: {
	    name: 'Category', /* will be replaced with real category name */
	    meta: {
	        null: {
	        }
	    },
	    elements: {
	        name: 'SubCategory',
	        path: 'section.category-products', /* we can just point to the document's body if there are no sub categories */
	        meta: {
	            name: {
	                path: 'h2',
	                func: 'text'
	            }
	        },
	        elements: {
	            name: 'Products',
	            path: "li[itemtype='http://schema.org/Product']",
	            meta: {
	                id: {
	                    attr: 'id'
	                },
	                location: {
	                    path: 'a',
	                    attr: 'href'
	                },
	                title: {
	                    path: '.product-name',
	                    func: 'text',
	                },
	                description: {
	                    path: '.short-description',
	                    func: 'text'
	                },
	                image: {
	                    path: "img[itemprop='image']",
	                    attr: 'src'
	                },
	                price: {
	                    path: '.price',
	                    func: 'text'
	                }
	            }
	        }
	    }
	}
};


/*
* verify our crawler works and returns what we expect
*/
describe("Test Crawler specs: ", function() {
	var crawler = require('../lib/crawler.js').getCrawler( { debug: false } );

	it("crowler.getBrowser() should return browser of the crawler", function(done) {
		expect( crawler.getBrowser() ).toBeDefined();
		done();
	});

	it("crawler should return jsdom (jQuery) after we visit the site", function( done ) {
		crawler.getDom( site, function( err, $) {
			expect( err ).toBeNull(); //no errors :)
			expect( $ ).toBeDefined(); 
			expect( $.fn.jquery ).toBeDefined();
			done();
		});
	});

	it("crowler should return an error if navigation config is not defined", function(done) {
		crawler.getNavigation( function(err, navigation ) {
			//console.log( navigation );
			expect( err ).not.toBeNull(); //no errors :)
			done();
		});
	});
});


/*
* we can ask crawler to automatically discover possible categories
*/
describe("Test Crawler for auto navigation specs: ", function() {

	//initialize with navigation only config.
	var crawler = require('../lib/crawler.js').getCrawler( navigation_only_config );

	it("crawler.getNavigation() should return an object literal where the elements property contains a list/array of categories", function( done ) {
		crawler.getNavigation( function(err, navigation ) {
			//console.log( navigation );
			expect( err ).toBeNull(); //no errors :)
			expect(navigation.meta.length).toBeGreaterThan(2); //we should have at least 2 categories
			done();
		});
	});

	it("crawler.getNavigation() should return an object literal containing and array of categories where the first category name is 'New'", function( done ) {
		crawler.getNavigation( function(err, navigation ) {
			expect( err ).toBeNull(); //no errors :)
			expect(navigation.meta[0].name).toMatch('New');
			done();
		});
	});

	it("crawler.getNavigation() should return an object literal containing an array categories whith one named 'Maide Golf'", function( done ) {
		crawler.getNavigation( function(err, navigation ) {
			expect( err ).toBeNull(); //no errors :)
			expect( _.pluck( navigation.meta, 'name') ).toContain('Maide Golf');
			done();
		});
	});

});

describe("Test Crawler discover one category for products: ", function() {
	//we try to explore just one page
	var crawler = require('../lib/crawler.js').getCrawler( full_config );

	var category_name = 'Accessories';
	var href = '/b/accessories-for-men';

	it("crawler.discoverCategory() for 'Accesories' category should return more than one subcateogry", function(done) {
		crawler.discoverCategory( href, category_name, function( err, result ) {
			expect( err ).toBeNull(); //no errors :)
			//console.log( JSON.stringify( result ) );
			//more than one subcategory
			expect( result.meta[0].elements.meta.length ).toBeGreaterThan( 1 );
			//Subcategory Belts
			done();
		});
	});

	it("crawler.discoverCategory() for 'Accesories' category should should contain subcategory 'Belts'", function(done) {
		crawler.discoverCategory( href, category_name, function( err, result ) {
			expect( _.pluck(result.meta[0].elements.meta, 'name' ) ).toContain( 'Belts' );
			done();
		});
	});


	it("crawler.discoverCategory() for 'Accesories' category should return more than one products", function(done) {
		crawler.discoverCategory( href, category_name, function( err, result ) {
			//More than one product
			expect( result.meta[0].elements.meta[0].elements.meta.length ).toBeGreaterThan( 1 );
			done();
		});
	});


});

describe("Test Crawler to try to discover products for non category page: ", function() {
	//we try to explore just one page
	var crawler = require('../lib/crawler.js').getCrawler( full_config );

	var category_name = 'Gifts';
	var href = '/giftguide';

	it("crawler.discoverCategory() for 'Gifts' page should return Zero subcategories and products", function(done) {
		crawler.discoverCategory( href, category_name, function( err, result ) {
			expect( err ).toBeNull(); //no errors :)
			//console.log( JSON.stringify( result ) );
			//more than one subcategory
			expect( result.meta[0].elements.meta.length ).toBe( 0 );
			done();
		});
	});

});


/*
TODO: becasue of the timeout(too much time to discover each category), I will need to shut down the timout function here.
*/
xdescribe("Crawler all products discovery", function() {
	var crawler = require('../lib/crawler.js').getCrawler( full_config );
	it("crawler.crawl should go through each category and return results", function(done) {
		crawler.crawl(function(err, results) {
			expect(err).toBeNull();
			console.log( JSON.stringify( results ) );
			done();
		});
	});
});