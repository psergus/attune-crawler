# Artifacts

## Install

First, you need to install nodejs. Go to http://www.nodejs.org and follow instructions. Then you clode the crawler's git and install required libraries in 
the folder:

```bash
$ npm install
```
And then run the script:

```bash
$ node app.js
```

The output will be a master list of products from all collections. Each product includes id, location, title, price, description, sub category, and category.
Using lodash or underscore JS libraries, the list can be easily converted into groups of categories and/or subcategories. We can also perform search for 
unique products or products that appear more than once in different collections.

## Approach:

It would be the right approach to take when considering a crawler written on JavaScript because the language is so natural fit to interact with Web pages. 
Moreover, existing libraries such as Sizzle, jQuery, or even a native DOM selectors are quick and organic approaches to grab info from the markup.
To build a crawler, I researched existing nodejs crawler modules. 
Unfortunately, many of them were either in a raw stage or not compliant with the dynamic content loading, which Bonabos does.
To tuckle the problem, I found two great libraries: ZombieJS and PhantomJS, which provide a headless browser 
that is able to work with dynamic loading content. My choice fell upon ZombieJS because this library is more mature and lighter in terms of installation.
PhantomJS requires C++, C, Shell, and Javascript. 

## Improvements

For the future improvements, I would consider using parallel/async requests to all collections/categories. Right now, using parallel requests does not allow 
to grab the dynamic loading content, which I will need to research further. 
Config file language can and should be improved further to make the crawler universal to many e-commerce sites.

## Config

Inside the app.js, you will find 
```js
var config = { .... };
```
this is a config file to traverse and pick subcategories and products on the collection page. I created my own language for the config. It can be extended
to adopt different e-commerce sites. The main principle there is to define path to the element, and define which attributes we need to derive for an element.

## Test

```bash
$ npm install jasmine-node -g
$ jasmine-node ./spec
```
Per my multiple tests, Bonobos.com does not produce consistent results on its pages. For example, sometimes it returns one subcategory on Accessories page. 

